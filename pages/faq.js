import Head from 'next/head'
import Link from 'next/link'
import { useEffect, useState } from 'react'
import FAQScreen from '../src/screens/FAQScreen';

//export async function getServerSideProps() { } => roda a cada acesso que vc recebe na página
//          esse metódo garante que em caso de utilização de dados de uma API, eles serão
//          atualziados caso a API seja atualizada. Só use em caso de necessidade de dados dinâmicos
// precisa ser buildado: yarn build && yarn start

export async function getStaticProps() { 
    // prod:roda uma unica vez durante o build, mas não garante que em caso de atualização de dados
    //       da API utilizada, os dados não seram atualziados proporcionalmente
    //dev: roda sempre
    // Ajuda quanto a SEO pois ja traz dados importantes injetados na página e ajuda na indexação
    const FAQ_API_URL = 'https://gist.githubusercontent.com/omariosouto/0ceab54bdd8182cbd1a4549d32945c1a/raw/578ad1e8e5296fa048e3e7ff6b317f7497b31ad9/alura-cases-faq.json';
    const faq = await fetch(FAQ_API_URL)
            .then((responseServer)=> {
                return responseServer.json()
            })
            .then((response)=> {
                return response
            });

    return {
        props: {
            faq
        }, //will be passed to the page component as prop
    };
}

export default FAQScreen;