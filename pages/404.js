import Link from 'next/link'

export default function Page404() {
    return(
        <>
            <h1>Ops...! o que você procura não esta por aqui no momento</h1>
            <Link href="/" passHref>Ir para a Home</Link>
        </>
    )
}